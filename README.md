## Quick Start

1. Install [`Nextflow`](https://www.nextflow.io/docs/latest/getstarted.html#installation) (`>=21.10.3`)

2. Install any of [`Docker`](https://docs.docker.com/engine/installation/)

3. Download the pipeline and test it on a minimal dataset with a single command:

    ```console
    nextflow run labmedinformatics/covid-seq-pipeline -profile test
    ```

4. Start running your own analysis!

    * Typical command for Illumina amplicon analysis:

        ```bash
        nextflow run labmedinformatics/covid-seq-pipeline \
            --input samplesheet.csv \
            --platform illumina \
            --protocol amplicon \
            --genome 'MN908947.3' \
            --primer_set artic \
            --primer_set_version 3 \
            --skip_assembly \
            -profile <docker/singularity/podman/conda/institute>
        ```