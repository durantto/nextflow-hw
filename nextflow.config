/*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    labmedinformatics/covid-seq-pipeline Nextflow config file
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Default config options for all compute environments
    https://github.com/nf-core/viralrecon/blob/master/nextflow.config
----------------------------------------------------------------------------------------
*/

manifest {
    name                        = 'labmedinformatics/covid-seq-pipeline'
    author                      = 'Thomas Durant and Ajay Arora'
    homePage                    = 'https://cchteam.med.yale.edu/gitlab/labmedinformatics/covid-seq-pipeline'
    description                 = 'Assembly and intrahost/low-frequency variant calling for viral samples'
    mainScript                  = 'main.nf'
    nextflowVersion             = '>=21.10.6'
    version                     = '1.0.0'
}


// Global default parameters
params {

    // Input
    batch_id                    = null
    spec_id                     = "*"
    fastq_file_ext              = "fastq.gz"
    primer_R1_suffix            = "R1"
    primer_R2_suffix            = "R2"
    fastq_path                  = "$baseDir/data/${params.batch_id}/${params.spec_id}_{${primer_R1_suffix},${primer_R2_suffix}}.${params.fastq_file_ext}"

    // Output
    // $group/$technology/$assay/$case_num
    results                     = "$baseDir/illumina/COVID_SEQ_${manifest.version}/BATCH_${params.batch_id}/"

    // Reference
    ref_fasta                   = "$baseDir/resources/ref_genome/nCoV-2019.reference.fasta"
    primer_bed                  = "$baseDir/resources/primers/ARTIC_v3_nCoV-2019_primer_TJD.bed"
    pangolin_datadir_tag        = "02152022"
    pangolin_datadir            = "$baseDir/resources/pangolin/${params.pangolin_datadir_tag}/"

    // Publish Options
    publish_dir_mode            = 'copy'

    // bin
    depth_of_coverage_py        = "$baseDir/bin/percent_depth_of_coverage.py"

    // Max resource options
    // Defaults only, expecting to be overwritten
    // max_memory                  = '128.GB'
    // max_cpus                    = 16
    // max_time                    = '240.h'
}



// Profile declarations
profiles {

    // Docker profile
    docker {
        docker {
            enabled = true
            //runOptions = '-u $(id -u):$(id -g)'
            }
            process {
            withLabel : fastqc {
                // container = "registry.gitlab.com/durantto/nextflow-hw/fastqc:latest"
                container = "cchteam.med.yale.edu:443/labmedinformatics/bioinformatics-library/fastqc:685159bc"
            }
            withLabel : ivar {
                // container = "registry.gitlab.com/durantto/nextflow-hw/ivar:latest"
                container = "cchteam.med.yale.edu:443/labmedinformatics/bioinformatics-library/ivar:685159bc"
            }
            withLabel : pangolin {
                container = "registry.gitlab.com/durantto/nextflow-hw/pangolin:13a3e465"
                // container = "registry.gitlab.com/durantto/pangolin/pangolin:36acf0ed"
                // container = "cchteam.med.yale.edu:443/labmedinformatics/bioinformatics-library/pangolin:685159bc"
                // container = "cchteam.med.yale.edu:443/labmedinformatics/docker-pangolin-clia/pangolin-clia:13a3e465"
            }
            withLabel : python {
                // container = "registry.gitlab.com/durantto/nextflow-hw/python:latest"
                container = "cchteam.med.yale.edu:443/labmedinformatics/bioinformatics-library/biopython:685159bc"
            }
        }
    }

    test { 
        includeConfig 'conf/test.config'               
    }
}

