FROM python:3.9

COPY deploy/requirements.txt ./

RUN pip install --upgrade pip
RUN pip install -U -r requirements.txt