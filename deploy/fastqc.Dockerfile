# Build this container from the official Ubuntu Image
# Make sure to use a tag that specifies version
# i.e. ubuntu:bionic-20181112 not just ubuntu
# The first line of your Dockerfile must be a FROM instruction
FROM ubuntu:bionic-20181112

# Add some metadata to describe our container
LABEL maintainer="aj"
LABEL maintainer_email="aj@cisinc.org"
LABEL version="1.0"

# The ADD command will fetch and unzip the fastqc software
ADD https://www.bioinformatics.babraham.ac.uk/projects/fastqc/fastqc_v0.11.8.zip /

# We will run some commands just as we did in bash
# Notice that we use the -y option for our apt-get since we can't say y during the build process
# We also want to run the apt-get update in the same line as our install
# We connect those commands with && to specify that the first command must exit
# successfully before the second command is executed
# We won't install wget since the ADD command eliminates the need

RUN apt-get update -y && apt-get install -y\
 default-jdk\
 libfindbin-libs-perl\
 unzip

# We will modify the permission on the software and create our symbolic link

RUN unzip /fastqc_v0.11.8.zip && chmod 755 /FastQC/fastqc && ln -s /FastQC/fastqc /bin

# We will define fastqc as the application to run when this container starts
# ENTRYPOINT ["fastqc"]