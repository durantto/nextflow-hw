# Build this container from the official Ubuntu Image
# Make sure to use a tag that specifies version
# i.e. ubuntu:xenial16.04.7 not just ubuntu
# The first line of your Dockerfile must be a FROM instruction
FROM ubuntu:xenial-20160525

# Add some metadata to describe our container
LABEL maintainer="Ajay Arora and Thomas Durant"
LABEL maintainer_email="ajay.arora@yale.edu"
LABEL version="1.0"

# ARG variables only persist during build time
# had to include the v for some of these due to GitHub tags.
# USHER ver is in the conda version style.
# thankfully pangolearn github tag is simply a date

# Pandolin Software and Software Dependencies
ARG PANGOLIN_VER="v3.1.20"
ARG SCORPIO_VER="v0.3.16"
ARG USHER_VER="0.5.3"

# Pangolin Data Dependencies
ARG PANGOLEARN_VER="2021-07-28"
ARG CONSTELLATIONS_VER="v0.0.11"
ARG PANGO_DESIGNATION_VER="v1.2.52"


LABEL base.image="ubuntu:xenial"
LABEL dockerfile.version="1"
LABEL software="pangolin"
LABEL software.version=${PANGOLIN_VER}
LABEL description="Conda environment for Pangolin. Pangolin: Software package for assigning SARS-CoV-2 genome sequences to global lineages."
LABEL website="https://github.com/cov-lineages/pangolin"
LABEL license="GNU General Public License v3.0"
LABEL license.url="https://github.com/cov-lineages/pangolin/blob/master/LICENSE.txt"

# Install needed software for conda install; cleanup apt garbage
RUN apt-get update && apt-get install -y \
 wget \
 git \
 build-essential && \
 apt-get autoclean && rm -rf /var/lib/apt/lists/*

# Get miniconda and the pangolin repo
RUN wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh && \
 bash ./Miniconda3-latest-Linux-x86_64.sh -p /miniconda -b  && \
 rm Miniconda3-latest-Linux-x86_64.sh && \
 wget "https://github.com/cov-lineages/pangolin/archive/${PANGOLIN_VER}.tar.gz" && \
 tar -xf ${PANGOLIN_VER}.tar.gz && \
 rm ${PANGOLIN_VER}.tar.gz && \
 mv -v pangolin-* pangolin

# Set the environment
ENV PATH="/miniconda/bin:$PATH" \
 LC_ALL=C

# Modify environment.yml to pin specific versions during install
# Create the conda environment using modified environment.yml and set as default
RUN sed -i "s|usher.*|usher=${USHER_VER}|" /pangolin/environment.yml && \
 sed -i "s|pangoLEARN.git|pangoLEARN.git@${PANGOLEARN_VER}|" /pangolin/environment.yml && \
 sed -i "s|scorpio.git|scorpio.git@${SCORPIO_VER}|" /pangolin/environment.yml && \
 sed -i "s|constellations.git|constellations.git@${CONSTELLATIONS_VER}|" /pangolin/environment.yml && \
 sed -i "s|pango-designation.git|pango-designation.git@${PANGO_DESIGNATION_VER}|" /pangolin/environment.yml && \
 conda env create -f /pangolin/environment.yml && \
 echo "source activate pangolin" > /etc/bash.bashrc

ENV PATH /miniconda/envs/pangolin/bin:$PATH

# Took this out from below step: python setup.py install (seems like the install instructions changed?)
# replaced with pip install .
RUN cd pangolin && \
 pip install . && \
 conda clean -a -y && \
 mkdir /data && \
 pangolin -v && pangolin -pv && pangolin -dv

WORKDIR /data