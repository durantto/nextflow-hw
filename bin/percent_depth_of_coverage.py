#!/usr/bin/env python
from __future__ import division
import csv
import sys

DEPTH_THRESHOLD = 50
PASS_DEPTH_THRESHOLD = []

def depth_of_coverage_ratio(x,y):
    try:
        return round(x/y,2)
    except ZeroDivisionError:
        return None

i=0

with open(sys.argv[1]) as file:
       
    tsv_file = csv.reader(file, delimiter="\t")

    for line in tsv_file:

        i+=1
        
        if int(line[2]) >= DEPTH_THRESHOLD:
        
            PASS_DEPTH_THRESHOLD.append(line[2])


depth_of_coverage = depth_of_coverage_ratio(len(PASS_DEPTH_THRESHOLD), i)

print (depth_of_coverage)

output_file = sys.argv[2]+"_percent_depth_of_coverage.tsv"

with open(output_file, 'w') as csvfile:
    my_writer = csv.writer(csvfile, delimiter = '\t')
    my_writer.writerow([depth_of_coverage])
