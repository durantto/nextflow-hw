#!/bin/bash

#Unique name given to each library. This will be provided in a list file for each library.  
file_base=$1

#Creates a log for each processing step for each library 
log=${file_base}.metagenomic.pipeline.log

{

echo "***********************************" 
echo "begin metagenomic consensus generation: $file_base" 
echo "***********************************" 



#variables set of each read
f1=/workdir/01_DATA/12345/${file_base}_R1.fastq.gz
f2=/workdir/01_DATA/12345/${file_base}_R2.fastq.gz

reference_genome=/workdir/01_DATA/ref_genome/nCoV-2019.reference.fasta


echo "**********************************"
echo "Total reads in metagenomic sample: $file_base"
echo $(cat $f1 | wc -l)/4*2|bc
echo "**********************************"

#Align paired end reads to the reference covid virus genome
bwa mem -t 16 $reference_genome $f1 $f2 | samtools view -b -F 4 -F 2048 | samtools sort -o ${file_base}_aln_sorted.bam

#index for consensus generation
samtools index ${file_base}_aln_sorted.bam

#Use samtools to identify variants in reads compared to references, call consensus sequence with ivar
samtools mpileup -A -d 0 -Q 0 ${file_base}_aln_sorted.bam | ivar consensus -t 0.75 -p ${file_base}_consensus

#Use samtools to identify variants in reads compared to reference, output variants in tsv
samtools mpileup -A -d 0 -Q 0 ${file_base}_aln_sorted.bam | ivar variants -p ${file_base}_variants -q 20 -t 0.1 -r $reference_genome

#generate Depth of Coverage file for each position
samtools depth -a ${file_base}_aln_sorted.bam > ${file_base}_DOC.txt

#total reads that remain after removing human reads
echo "**********************************"
echo "Total reads that align to reference for sample: $file_base"
samtools view -c -F 260 ${file_base}_aln_sorted.bam
echo "**********************************"

echo "***********************************" 
echo "finished metagenomic consensus generation: $file_base" 
echo "***********************************" 

#finish log file for pipeline 
} 2>&1  | tee -a $log