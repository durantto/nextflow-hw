#!/usr/bin/env nextflow

/*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    labmedinformatics/covid-seq-pipeline
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Gitlab : https://cchteam.med.yale.edu/gitlab/labmedinformatics/covid-seq-pipeline
----------------------------------------------------------------------------------------
*/


Channel
  .fromFilePairs(params.fastq_path, checkIfExists: true, flatten: false)
  .into{reads_ch; reads_ch_2}


// Run FASTQC
process fastqc {

  tag "$sample_id"

  // Link process to container via label
  label "fastqc"

  publishDir "${params.results}/${sample_id}/", mode : params.publish_dir_mode, overwrite : true 

  input:
  tuple val(sample_id), file(fastq_file) from reads_ch
  
  output:
  file "${sample_id}*_fastqc.{zip,html}" into fastqc_ch
  
  script:
  """
  fastqc -t 8 ${fastq_file}
  """
}


process align_to_reference {

  tag "$sample_id"

  // Link process to container via label
  label "ivar"

  publishDir "${params.results}/${sample_id}/", mode : params.publish_dir_mode, overwrite : true 

  input:
  tuple val(sample_id), file(fastq_file) from reads_ch_2 
  path(params.ref_fasta)

  output:
  tuple(sample_id), path('*_aln.bam') into aln_bam_ch
  
  script:
  """
  # Align paired end reads to the reference covid virus genome
  # view converts sam to bam
  # sort is a required as iVar takes a sorted bam file
  bwa mem -t 16 ${params.ref_fasta} ${fastq_file} | samtools view -b -F 4 -F 2048 | samtools sort -o ${sample_id}_aln.bam
  """

}

process trim_primers {

  tag "$sample_id"

  // Link process to container via label
  label "ivar"

  publishDir "${params.results}/${sample_id}/", mode : params.publish_dir_mode, overwrite : true 

  input:
  tuple val(sample_id), path(aln_bam_file) from aln_bam_ch
  path(params.primer_bed)
  
  output:
  tuple(sample_id), path('*.bam') into aln_trimmed_bam_ch
  tuple(sample_id), path('*.bai') into aln_trimmed_bai_ch
  
  script:
  """
  #Perform QC and soft clip primer sequences 
  ivar trim -e -i ${aln_bam_file} -b ${params.primer_bed} -p ${sample_id}_aln_trimmed.bam 
  """

}


process sort_index_bam {

  tag "$sample_id"

  // Link process to container via label
  label "ivar"
  
  publishDir "${params.results}/${sample_id}/", mode : params.publish_dir_mode, overwrite : true 

  input:
  tuple val(sample_id), path(aln_trimmed_bam_file) from aln_trimmed_bam_ch

  output:
  tuple(sample_id), path('*') into (aln_trimmed_sorted_ch, aln_trimmed_sorted_ch_2, aln_trimmed_sorted_ch_3)

  script:
  """
  # Sort output of ivar 
  samtools sort ${aln_trimmed_bam_file} -o ${sample_id}_aln_trimmed_sorted.bam 
  """

}


process generate_consensus {

  tag "$sample_id"

  // Link process to container via label
  label "ivar"

  publishDir "${params.results}/${sample_id}/", mode : params.publish_dir_mode, overwrite : true 

  input:
  tuple val(sample_id), path(aln_trimmed_sorted) from aln_trimmed_sorted_ch

  output:
  tuple(sample_id), path('*.fa') into (consensus_ch, consensus_ch_ruo)

  script:
  """
  # Use samtools to identify variants in reads compared to references, call consensus sequence with ivar 
  samtools mpileup -A -d 1000 -Q 0 ${aln_trimmed_sorted} | ivar consensus -t 0.60 -m 20 -p ${sample_id}_consensus
  """

}


process variants_to_tsv {

  tag "$sample_id"

  // Link process to container via label
  label "ivar"

  publishDir "${params.results}/${sample_id}/", mode : params.publish_dir_mode, overwrite : true 

  input:
  tuple val(sample_id), path(aln_trimmed_sorted) from aln_trimmed_sorted_ch_2
  path(params.ref_fasta)

  output:
  tuple(sample_id), path('*.{tsv,txt}') into (variants_ch)

  script:
  """
  #Use samtools to identify variants in reads compared to reference, output variants in tsv
  samtools mpileup -A -d 0 -Q 0 ${aln_trimmed_sorted} | ivar variants -p ${sample_id}_variants -q 20 -t 0.02 -r ${params.ref_fasta}

  awk -F '\t' '(0.2 < \$11 && \$11 < 0.80) && (\$14=="TRUE")' ${sample_id}_variants.tsv > ${sample_id}_frequency.tsv

  wc -l ${sample_id}_frequency.tsv > ${sample_id}_frequency_count.txt
  """
}


process depth_of_coverage {

  tag "$sample_id"

  // Link process to container via label
  label "ivar"

  publishDir "${params.results}/${sample_id}/", mode : params.publish_dir_mode, overwrite : true 

  input:
  tuple val(sample_id), path(aln_trimmed_sorted) from aln_trimmed_sorted_ch_3

  output:
  tuple(sample_id), path('*') into (depth_of_cov_ch)

  script:
  """
  # Generate Depth of Coverage file for each position
  samtools depth -a ${aln_trimmed_sorted} > ${sample_id}_depth_of_coverage.tsv
  """
}


process lineage_call_clia {

  tag "$sample_id"

  // Link process to container via label
  label "pangolin"

  publishDir "${params.results}/${sample_id}/", mode : params.publish_dir_mode, overwrite : true 

  input:
  tuple val(sample_id), path(consensus_fa) from consensus_ch
  path(params.pangolin_datadir)
  
  output:
  tuple(sample_id), path('*.csv') into (lineage_call_clia_ch)

  script:
  """
  # Make lineage call with specific pangolin reference data files
  pangolin ${consensus_fa} --datadir ${params.pangolin_datadir} --outfile ${sample_id}_clia2.csv
  """
}


process lineage_call_ruo {

  tag "$sample_id"

  // Link process to container via label
  label "pangolin"

  publishDir "${params.results}/${sample_id}/", mode : params.publish_dir_mode, overwrite : true 

  input:
  tuple val(sample_id), path(consensus_fa) from consensus_ch_ruo

  output:
  tuple(sample_id), path('*.csv') into (lineage_call_ruo_ch)

  script:
  """
  # Make lineage call with specific pangolin reference data files
  pangolin ${consensus_fa} --outfile ${sample_id}_ruo.csv
  """
  
}


process percent_depth_of_coverage {
  
  tag "$sample_id"
  
  label "python"
  
  publishDir "${params.results}/${sample_id}/", mode : params.publish_dir_mode, overwrite : true 

  input:
  tuple val(sample_id), path(depth_of_cov_tsv) from depth_of_cov_ch
  file(params.depth_of_coverage_py)

  output:
  tuple(sample_id), path('*.tsv') into percent_doc_ch

  script:
  """
  ${params.depth_of_coverage_py} ${depth_of_cov_tsv} ${sample_id}
  """
}
